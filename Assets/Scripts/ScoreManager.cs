﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static int score = 0;
	public static int highScore = 10;
	// Use this for initialization
	void Start () 
	{
		score = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	public static void AddScore()
	{
		score++;
	}
	public static void ResetScore()
	{
		score = 0;
	}
	public static void SetHighScore(int myScore)
	{
		highScore = myScore;
	}
}
